package com.cyprias.NetherPortalRescue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.cyprias.NetherPortalRescue.configuration.Config;
import com.cyprias.NetherPortalRescue.configuration.YML;

import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class Plugin extends JavaPlugin {
	private static Plugin instance = null;
	public static String chatPrefix = "&4[&bNPR&4]&r ";

	public static HashMap<String, Location> deaths = new HashMap<String, Location>();

	public void onEnable() {
		instance = this;

		// Check if config.yml exists on disk, copy it over if not. This keeps
		// our comments intact.
		if (!(new File(getDataFolder(), "config.yml").exists())) {
			Logger.info(chatPrefix + "Copying config.yml to disk.");
			try {
				YML.toFile(getResource("config.yml"), getDataFolder(), "config.yml");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		// Check if the config on disk is missing any settings, tell console if
		// so.
		try {
			Config.checkForMissingProperties();
		} catch (IOException e4) {
			e4.printStackTrace();
		} catch (InvalidConfigurationException e4) {
			e4.printStackTrace();
		}

		// Register our event listener.
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);

		// Start the Metrics.
		if (Config.getBoolean("properties.use-metrics"))
			try {
				Metrics metrics = new Metrics(this);
				metrics.start();
			} catch (IOException e) {
			}
	}

	public static final Plugin getInstance() {
		return instance;
	}

	public static int scheduleSyncDelayedTask(Runnable arg1, long delay) {
		return instance.getServer().getScheduler().scheduleSyncDelayedTask(instance, arg1, delay);
	}

	public final class PlayerListener implements Listener {
		@EventHandler
		public void onPlayerJoinEvent(PlayerJoinEvent e) {
			final Player p = e.getPlayer();
			if (Config.getBoolean("properties.login-check") && p.getLocation().getBlock().getType() == Material.PORTAL) {
				Logger.debug(p.getName() + " logged in inside a portal block, sending them to spawn...");
				Plugin.scheduleSyncDelayedTask(new Runnable() {
					@Override
					public void run() {
						p.teleport(Plugin.getInstance().getServer().getWorlds().get(0).getSpawnLocation());
						ChatUtils.send(p, chatPrefix + Config.getString("messages.ReturnedToSpawn"));

					}
				}, 2L);
			}

		}

		@EventHandler
		public void onPlayerPortalEvent(PlayerPortalEvent e) {
			if (e.getCause().equals(TeleportCause.NETHER_PORTAL)) {
				if (Config.getBoolean("properties.auto-rescue")) {
					final Player p = e.getPlayer();
					final Location from = e.getFrom();

					Plugin.scheduleSyncDelayedTask(new Runnable() {
						@Override
						public void run() {
							if (!p.isOnline())
								return;

							if (p.getLocation().getBlock().getType() == Material.PORTAL) {
								Logger.debug(p.getName() + " is still in a portal, sending them back.");
								ChatUtils.send(p, chatPrefix + Config.getString("messages.ReturnToPrevLoc"));
								p.teleport(from);
							}

						}
					}, Config.getInt("properties.wait-timer") * 20L);

				}
			}

		}
	}


}
