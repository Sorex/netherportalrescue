package com.cyprias.NetherPortalRescue.configuration;

import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;

import com.cyprias.NetherPortalRescue.Logger;
import com.cyprias.NetherPortalRescue.Plugin;

public class Config {
	public static boolean getBoolean(String property) {
		return Plugin.getInstance().getConfig().getBoolean(property);
	}

	public static String getString(String property) {
		return Plugin.getInstance().getConfig().getString(property);
	}

	public static Integer getInt(String property) {
		return Plugin.getInstance().getConfig().getInt(property);
	}

	public static void checkForMissingProperties() throws IOException, InvalidConfigurationException {
		YML diskConfig = new YML(Plugin.getInstance().getDataFolder(), "config.yml");
		YML defaultConfig = new YML(Plugin.getInstance().getResource("config.yml"));

		for (String property : defaultConfig.getKeys(true)) {
			if (!diskConfig.contains(property))
				Logger.warning(Plugin.chatPrefix + property + " is missing from your config.yml, using default.");
		}

	}
}
